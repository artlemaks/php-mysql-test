<div class="ps-content-wrapper-v0">
<div class="preheader" style="display:none;">Query a list of professor names and the respective courses they teach (or have taught).</div>

<p>A university maintains data on professors, departments, courses, and schedules in four tables: <em>DEPARTMENT</em>, <em>PROFESSOR</em>, <em>COURSE</em>, and <em>SCHEDULE</em>.</p>

<p>&nbsp;</p>

<p>Write a query to print the names of professors with the names of the courses they teach (or have taught) <em>outside of their department</em>. Each row in the results must be distinct (i.e., a professor teaching the same course over multiple semesters should only appear once), but the results can be in any order.&nbsp; Output should contain two columns:&nbsp;<em>PROFESSOR.NAME, COURSE.NAME</em>&nbsp;.</p>

<p>&nbsp;</p>

<details><summary class="section-title">Schema</summary>

<div class="collapsable-details">
<div style="width:100%;display:inline-block;">
<table style="float:left;margin:4px;width:48%;">
	<tbody>
		<tr>
			<th colspan="3">PROFESSOR</th>
		</tr>
		<tr>
			<th style="white-space:nowrap;">Name</th>
			<th style="white-space:nowrap;">Type</th>
			<th style="white-space:nowrap;">Description</th>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">ID</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>A professor's ID in the inclusive range <em>[1, 1000]</em>. This is a <em>primary key</em>.</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">NAME</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">String</td>
			<td>A professor's name. This field contains between <em>1</em> and <em>100</em> characters (inclusive).</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">DEPARTMENT_ID</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>A professor's department ID. This is a <em>foreign key</em> to <em>DEPARTMENT.ID</em>.</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">SALARY</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>A professor's salary in the inclusive range <em>[5000, 40000]</em>.</td>
		</tr>
	</tbody>
</table>

<table style="float:left;margin:4px;width:48%;">
	<tbody>
		<tr>
			<th colspan="100">DEPARTMENT</th>
		</tr>
		<tr>
			<th style="white-space:nowrap;">Name</th>
			<th style="white-space:nowrap;">Type</th>
			<th style="white-space:nowrap;">Description</th>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">ID</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>A department ID in the inclusive range <em>[1, 1000]</em>. This is a <em>primary key</em>.</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">NAME</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">String</td>
			<td>A department name. This field contains between <em>1</em> and <em>100</em> characters (inclusive).</td>
		</tr>
	</tbody>
</table>
</div>

<p style="clear:both;">&nbsp;</p>

<div style="width:100%;display:inline-block;">
<table style="float:left;margin:4px;width:48%;">
	<tbody>
		<tr>
			<th colspan="3">COURSE</th>
		</tr>
		<tr>
			<th style="white-space:nowrap;">Name</th>
			<th style="white-space:nowrap;">Type</th>
			<th style="white-space:nowrap;">Description</th>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">ID</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>A course ID in the inclusive range <em>[1, 1000]</em>. This is a <em>primary key</em>.</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">NAME</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">String</td>
			<td>A course name. This field contains between <em>1</em> and <em>100</em> characters (inclusive).</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">DEPARTMENT_ID</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>A course's department ID. This is a <em>foreign key</em> to <em>DEPARTMENT.ID</em>.</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">CREDITS</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>The number of credits allocated to the course in the inclusive range <em>[1, 10]</em>.</td>
		</tr>
	</tbody>
</table>

<table style="float:left;margin:4px;width:48%;">
	<tbody>
		<tr>
			<th colspan="3">SCHEDULE</th>
		</tr>
		<tr>
			<th style="white-space:nowrap;">Name</th>
			<th style="white-space:nowrap;">Type</th>
			<th style="white-space:nowrap;">Description</th>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">PROFESSOR_ID</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>The ID of the professor teaching the course. This is a <em>foreign key</em> to <em>PROFESSOR.ID</em>.</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">COURSE_ID</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>The course's ID number. This is a <em>foreign key</em> to <em>COURSE.ID</em>.</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">SEMESTER</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>A semester ID in the inclusive range <em>[1, 6]</em>.</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">YEAR</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Integer</td>
			<td>A calendar year in the inclusive range <em>[2000, 2017]</em>.</td>
		</tr>
	</tbody>
</table>
</div>

<p style="clear:both;">&nbsp;</p>

<p style="font-weight:bold;" title="The desired result of the query."><strong>Output Format</strong></p>

<p>Each distinct row of results must contain the name of a professor followed by the name of a course they taught outside of their department in the format:</p>

<p>&nbsp;</p>

<pre>PROFESSOR.NAME COURSE.NAME</pre>

<p>&nbsp;</p>

<p>&nbsp;</p>
</div>
</details>

<details><summary class="section-title">Sample Data Tables</summary>

<div class="collapsable-details">
<div style="width:100%;display:inline-block;">
<table style="float:left;margin:4px;width:48%;">
	<tbody>
		<tr>
			<th colspan="100">PROFESSOR</th>
		</tr>
		<tr>
			<th style="font-family:monospace;white-space:nowrap;">ID</th>
			<th style="font-family:monospace;white-space:nowrap;">NAME</th>
			<th style="font-family:monospace;white-space:nowrap;">DEPARTMENT_ID</th>
			<th style="font-family:monospace;white-space:nowrap;">SALARY</th>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Nancy Daniels</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">7169</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Billy Knight</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">9793</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Harry Myers</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">25194</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Antonio Rodriguez</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">9686</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Nicole Gome</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">30860</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">6</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Eugene George</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">10487</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">7</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Gloria Vasquez</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">6353</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">8</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Joyce Flores</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">25796</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">9</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Daniel Gilbert</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">35678</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">10</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Matthew Stevens</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">26648</td>
		</tr>
	</tbody>
</table>

<table style="float:left;margin:4px;width:48%;">
	<tbody>
		<tr>
			<th colspan="100">DEPARTMENT</th>
		</tr>
		<tr>
			<th>ID</th>
			<th>NAME</th>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Biological Sciences</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Technology</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">6</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Humanities &amp; Social Sciences</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Clinical Medicine</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Arts and Humanities</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Physical Sciences</td>
		</tr>
	</tbody>
</table>
</div>

<p style="clear:both;">&nbsp;</p>

<div style="width:100%;display:inline-block;">
<table style="float:left;margin:4px;width:48%;">
	<tbody>
		<tr>
			<th colspan="100">COURSE</th>
		</tr>
		<tr>
			<th style="font-family:monospace;white-space:nowrap;">ID</th>
			<th style="font-family:monospace;white-space:nowrap;">NAME</th>
			<th style="font-family:monospace;white-space:nowrap;">DEPARTMENT_ID</th>
			<th style="font-family:monospace;white-space:nowrap;">CREDITS</th>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">9</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Clinical Biochemistry</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Astronomy</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">6</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">10</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Clinical Neuroscience</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Pure Mathematics and Mathematical Statistics</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">6</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Geography</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">7</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">8</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Chemistry</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Physics</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">8</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Earth Science</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">7</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">7</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Materials Science and Metallurgy</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">Applied Mathematics and Theoretical Physics</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
		</tr>
	</tbody>
</table>

<table style="float:left;margin:4px;width:48%;">
	<tbody>
		<tr>
			<th colspan="100">SCHEDULE</th>
		</tr>
		<tr>
			<th style="font-family:monospace;">PROFESSOR_ID</th>
			<th style="font-family:monospace;">COURSE_ID</th>
			<th style="font-family:monospace;">SEMESTER</th>
			<th style="font-family:monospace;">YEAR</th>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2003</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2011</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">7</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2011</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">7</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">7</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2010</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">6</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2001</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">9</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2012</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">10</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">4</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2009</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2014</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">3</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2008</td>
		</tr>
		<tr>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">1</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">7</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">5</td>
			<td style="text-align:center;font-family:monospace;white-space:nowrap;">2007</td>
		</tr>
	</tbody>
</table>
</div>

<p style="clear:both;">&nbsp;</p>

<p style="font-weight:bold;">Sample Output</p>

<pre>Antonio Rodriguez Astronomy
Harry Myers Earth Sciences
Nancy Daniels Materials Science and Metallurgy
Gloria Vasquez Materials Science and Metallurgy
Antonio Rodriguez Geography
Daniel Gilbert Earth Sciences
Matthew Stevens Applied Mathematics and Theoretical Physics
Nancy Daniels Pure Mathematics and Mathematical Statistics
Nancy Daniels Applied Mathematics and Theoretical Physics
Nancy Daniels Materials Science and Metallurgy </pre>

<p>&nbsp;</p>

<p style="font-weight:bold;">Explanation</p>

<p>By referring to the sample data above, we can confirm that:</p>

<p>&nbsp;</p>

<ol>
	<li>Professor <em>Antonio Rodriguez</em>'s <em>department_id</em> is <em>3</em>, but the <em>Astronomy</em> course's <em>department_id</em> is <em>1</em>.</li>
	<li>Professor <em>Harry Myers</em>'s <em>department_id</em> is <em>4</em>, but the <em>Earth Sciences</em> course's <em>department_id</em> is <em>1</em>.</li>
	<li>Professor <em>Nancy Daniels</em>'s <em>department_id</em> is <em>4</em>, but the <em>Astronomy</em> course's <em>department_id</em> is <em>1</em>.</li>
	<li>Professor <em>Gloria Vasquez</em>'s <em>department_id</em> is <em>4</em>, but the <em>Materials Science and Metallurgy</em> course's <em>department_id</em> is <em>1</em>.</li>
	<li>Professor <em>Antonio Rodriguez</em>'s <em>department_id</em> is <em>3</em>, but the <em>Geography</em> course's <em>department_id</em> is <em>1</em>.</li>
	<li>Professor <em>Daniel Gilbert</em>'s <em>department_id</em> is <em>5</em>, but the <em>Earth Sciences</em> course's <em>department_id</em> is <em>1</em>.</li>
	<li>Professor <em>Matthew Stevens</em>'s <em>department_id</em> is <em>2</em>, but the <em>Applied Mathematics and Theoretical Physics</em> course's <em>department_id</em> is <em>1</em>.</li>
	<li>Professor <em>Nancy Daniels</em>'s <em>department_id</em> is <em>4</em>, but the <em>Pure Mathematics and Mathematical Statistics</em> course's <em>department_id</em> is <em>1</em>.</li>
	<li>Professor <em>Nancy Daniels</em>'s <em>department_id</em> is <em>4</em>, but the <em>Applied Mathematics and Theoretical Physics</em> course's <em>department_id</em> is <em>1</em>.</li>
	<li>Professor <em>Nancy Daniels</em>'s <em>department_id</em> is <em>4</em>, but the <em>Materials Science and Metallurgy</em> course's <em>department_id</em> is <em>1</em>.</li>
</ol>

<p>&nbsp;</p>
</div>
</details>
</div>
